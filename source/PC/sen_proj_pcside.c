//Steven Pesut
//This routine is used to send a character poll an AVR for data
//When a comma is sent to the AVR, GPS coordinates, temperature,
//and pressure values are returned as well as a flag that indicates
//whether the data should be logged or not.


#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

//Structure for holding GPS coords
typedef struct {
	short degrees;
	long degrees_dec;
} coordinates;

//Structure for holding temperature values
typedef struct {
	int degree;
	int decimal;
} temperature;

//Function declarations
//Setups a serial connection with /dev/ttyUSB0
int setup_serial(char *serialport);

//Gets a reading from the AVR
void get_reading(FILE *fp, temperature *temp, long *press, coordinates *lat, coordinates *longit, char *log_data);

//Stores data from the reading into a placemark in a kml file
void add_placemark(FILE *ptr, temperature *temp, long press, coordinates *lat, coordinates *longit);

//Initialize a kml header
void kml_init(FILE *ptr);

//Finalize a kml file
void kml_close(FILE *ptr);

int main (void)
{
	long pressure = 0;
	coordinates latitude, longitude;
	temperature temp_val;
	char log_flag=0;
	
	int fd;
	FILE *fp, *res;
	long int startt;
	
	fd = setup_serial("/dev/ttyUSB0");	//Creates a file descriptor for the device
	
	if (fd == -1) return -1;


	fp = fdopen(fd,"r+");			//Opens the device with read/write privleges.

	if (fp == NULL) {
		printf("Error opening file descriptor for reading.\n");
		return -1;
	}
	
	res = fopen("data.kml", "w");	//Opens a file for writing data

	if (res == NULL) {
		printf("Error opening file for writing.\n");
		return -1;
	}
	
	//Initialize the kml file
	kml_init(res);

	startt = (long int)time(NULL);		//Stores the start time of the program	
	
	//Doesn't log any data until switch is closed
	while (!log_flag) {
		get_reading(fp, &temp_val, &pressure, &latitude, &longitude, &log_flag);
		usleep(1000000);
	}
	
	//Logs data as long as the switch stays closed
	while(log_flag){
		if (((long int)time(NULL)-startt)%10 == 0) {	//Take data every 10 seconds
			
			get_reading(fp, &temp_val, &pressure, &latitude, &longitude, &log_flag);
			if (log_flag) add_placemark(res, &temp_val, pressure, &latitude, &longitude);
			usleep(1000000);
		}
	}
	
	//Finalize the kml file
	kml_close(res);
	return 0;
}


int setup_serial(char *serialport) {
	
	int newfd;
	struct termios tp_newserial;

	//Opens the serial port and checks for errors.
	newfd = open(serialport, O_RDWR|O_NOCTTY);	
	if (newfd < 0) {
		fprintf(stderr,"ERROR opening serial connection\n");
		return -1;
	}
	
	//Adjusts serial attributes to desired values
	tp_newserial.c_iflag = IGNPAR;	//Ignores parity
	tp_newserial.c_oflag = 0;
	tp_newserial.c_cflag = CS8 | CREAD | CLOCAL; //8 bit chars enable receiver no modem status lines
	tp_newserial.c_lflag =0 ;

   	//Sets the baud rate to 57600
	cfsetispeed(&tp_newserial, B57600);
	cfsetospeed(&tp_newserial, B57600);
	
	tcsetattr(newfd, TCSANOW, &tp_newserial);	//Sets the serial port to the necessary settings
	
	return newfd;	//Returns the file descriptor for the serial port
}

void get_reading(FILE *fp, temperature *temp, long *press, coordinates *lat, coordinates *longit, char *log_data)
{
	unsigned char temp0, temp1, temp_dec0, temp_dec1;
	unsigned char press0, press1, press2, press3;
	unsigned char lat_deg0, lat_deg1;
	unsigned char lat_deg_dec0, lat_deg_dec1, lat_deg_dec2, lat_deg_dec3;
	unsigned char long_deg0, long_deg1;
	unsigned char long_deg_dec0, long_deg_dec1, long_deg_dec2, long_deg_dec3;
	
	fprintf(fp, ",");			//Sends a comma to start the conversion
	
	fscanf(fp, "%c", &temp0);		//Reads in temperature values
	fscanf(fp, "%c", &temp1);
	fscanf(fp, "%c", &temp_dec0);
	fscanf(fp, "%c", &temp_dec1);
	
	fscanf(fp, "%c", &press0);		//Reads in pressure values
	fscanf(fp, "%c", &press1);
	fscanf(fp, "%c", &press2);
	fscanf(fp, "%c", &press3);
	
	fscanf(fp, "%c", &lat_deg0);		//Reads in latitude
	fscanf(fp, "%c", &lat_deg1);
	fscanf(fp, "%c", &lat_deg_dec0);
	fscanf(fp, "%c", &lat_deg_dec1);
	fscanf(fp, "%c", &lat_deg_dec2);
	fscanf(fp, "%c", &lat_deg_dec3);
	
	fscanf(fp, "%c", &long_deg0);		//Reads in longitude
	fscanf(fp, "%c", &long_deg1);
	fscanf(fp, "%c", &long_deg_dec0);
	fscanf(fp, "%c", &long_deg_dec1);
	fscanf(fp, "%c", &long_deg_dec2);
	fscanf(fp, "%c", &long_deg_dec3);
	
	fscanf(fp, "%c", log_data);		//Reads whether or not to log the data
		
	temp->degree = temp1;			//Reconstructs temperature value
	temp->degree = ((temp->degree)<<8)|temp0;
	temp->decimal = (temp_dec1<<8)|temp_dec0;
	
	*press = press3;			//Reconstructs pressure value
	*press = (*press<<8)|press2;
	*press = (*press<<8)|press1;
	*press = (*press<<8)|press0;
	
	lat->degrees = lat_deg1;		//Reconstructs latitude coords
	lat->degrees = (lat->degrees<<8)|lat_deg0;
	lat->degrees_dec = lat_deg_dec3;
	lat->degrees_dec = (lat->degrees_dec<<8)|lat_deg_dec2;
	lat->degrees_dec = (lat->degrees_dec<<8)|lat_deg_dec1;
	lat->degrees_dec = (lat->degrees_dec<<8)|lat_deg_dec0;
		
	longit->degrees = long_deg1;		//Reconstructs longitude coords
	longit->degrees = (longit->degrees<<8)|long_deg0;
	longit->degrees_dec = long_deg_dec3;
	longit->degrees_dec = (longit->degrees_dec<<8)|long_deg_dec2;
	longit->degrees_dec = (longit->degrees_dec<<8)|long_deg_dec1;
	longit->degrees_dec = (longit->degrees_dec<<8)|long_deg_dec0;
}

void kml_init(FILE *ptr)
{
	//Prints the header for a kml file to be used with google earth
	fprintf(ptr,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n<Document>\n");
}

void add_placemark(FILE *ptr, temperature *temp, long press, coordinates *lat, coordinates *longit)
{
	time_t rawtime;
	struct tm *timeinfo;
	
	//Takes the time of the measurement
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	
	//Puts each data point into the kml file as a placemark (pushpin)
	fprintf(ptr,"\t<Placemark>\n\t\t<name>\n\t\t\t%s\t\t</name>\n", asctime(timeinfo));
	fprintf(ptr,"\t\t<description>Temp: %3.2f Press: %4.2f</description>\n", (temp->degree*9/5. + temp->decimal*9/50000.)+32, press/100.);
	fprintf(ptr,"\t\t<Point>\n\t\t\t<coordinates>%d.%07lu,%d.%07lu,0</coordinates>\n\t\t</Point>\n\t</Placemark>\n",longit->degrees, longit->degrees_dec, lat->degrees, lat->degrees_dec);
	
	//Flush the stream to write the output immediately
	fflush(ptr);
}

void kml_close(FILE *ptr)
{
	//Closes the kml file
	fprintf(ptr,"</Document>\n</kml>");
}
