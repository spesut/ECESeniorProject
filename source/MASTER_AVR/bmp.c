#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <util/delay.h>
#include <avr/io.h>

#include "i2c.h"
#include "bmp.h"

//Sets up the over sampling rate
#define oss 0

void bmp_init(void)
{
	//Initiates the reading of the callibration values
	i2c_start();
	i2c_write(BMP_READ_ADDR);
	i2c_write(BMP_CAL_DAT_ADDR);
	i2c_repstart();
	i2c_write(BMP_WRITE_ADDR);
	
	//All calibration values are stored in global variables
	//Address is updated by the sensor, so values can be read continuously
	AC1 = i2c_read(1);
	AC1 = ((AC1 << 8) | i2c_read(1));
	AC2 = i2c_read(1);
	AC2 = ((AC2 << 8) | i2c_read(1));
	AC3 = i2c_read(1);
	AC3 = ((AC3 << 8) | i2c_read(1));
	AC4 = i2c_read(1);
	AC4 = ((AC4 << 8) | i2c_read(1));
	AC5 = i2c_read(1);
	AC5 = ((AC5 << 8) | i2c_read(1));
	AC6 = i2c_read(1);
	AC6 = ((AC6 << 8) | i2c_read(1));
	B1 = i2c_read(1);
	B1 = ((B1 << 8) | i2c_read(1));
	B2 = i2c_read(1);
	B2 = ((B2 << 8) | i2c_read(1));
	MB = i2c_read(1);
	MB = ((MB << 8) | i2c_read(1));
	MC = i2c_read(1);
	MC = ((MC << 8) | i2c_read(1));
	MD = i2c_read(1);
	MD = ((MD << 8) | i2c_read(0));
	i2c_stop();
}

long read_temp(void)
{
	long UT = 0;
	
	//Initiates a temperature reading
	i2c_start();
	i2c_write(BMP_READ_ADDR);
	i2c_write(BMP_CONROL_REG_ADDR);
	i2c_write(BMP_TEMP_CONTROL_REG);
	i2c_stop();
	
	//Checks the end of conversion (EOC) pin
	//done=1, still running=0
	EOC_DDR &= ~(1<<EOC_PIN);
	while (!(EOC_PIN_REG & (1<<EOC_PIN)));
	
	//Reads the two bytes of the temperature value
	i2c_start();
	i2c_write(BMP_READ_ADDR);
	i2c_write(BMP_DATA_REG_ADDR);
	i2c_repstart();
	i2c_write(BMP_WRITE_ADDR);
	
	//Values are shifted into type long and returned
	UT = i2c_read(1);
	UT = (UT << 8) | i2c_read(0);
	i2c_stop();
	return UT;
}

long read_press(void)
{
	long UP = 0;
	
	//Initiates a pressure reading
	i2c_start();
	i2c_write(BMP_READ_ADDR);
	i2c_write(BMP_CONROL_REG_ADDR);
	i2c_write(BMP_PRESS_CONTROL_REG + (oss<<6));
	i2c_stop();
	
	//Checks the end of conversion (EOC) pin
	//done=1, still running=0
	EOC_DDR &= ~(1<<EOC_PIN);
	while (!(EOC_PIN_REG & (1<<EOC_PIN)));
	
	//Reads values from the data register
	i2c_start();
	i2c_write(BMP_READ_ADDR);
	i2c_write(BMP_DATA_REG_ADDR);
	i2c_repstart();
	i2c_write(BMP_WRITE_ADDR);
	
	//Values are manipulated appropriately based on oss
	UP = i2c_read(1);
	UP = (UP << 8) | i2c_read(1);
	UP = (UP << 8) | i2c_read(0);
	UP >>= (8-oss);
	i2c_stop();
	
	return UP;
}

void calc_data(long *UT, long *UP)
{
	//Compensates values of temperature and pressure
	//utilizing the algorithm provided in the BMP085 Datasheet
	long X1, X2, B5, B6, X3, B3, p;
	unsigned long B4, B7;

	//Calculates Temperature and sets up values for pressure calc
	X1 = ((long) (*UT) - AC6)*AC5/32768;
	X2 = (long) MC*2048/(X1 + MD);
	B5 = X1 + X2;
	*UT = (B5 + 8)/16;
	
	//Calculates pressures
	B6 = B5 - 4000;
	X1 = (B2*(B6*B6 >> 12)) >> 11;
	X2 = AC2*B6 >> 11;
	X3 = X1 + X2;
	B3 = (((AC1*4+X3)<<oss) + 2)/4;
	X1 = AC3*B6 >> 13;
	X2 = (B1*(B6*B6 >> 12)) >> 16;
	X3 = ((X1 + X2) + 2) >> 2;
	B4 = AC4*((unsigned long)(X3+32768)) >> 15;
	B7 = ((unsigned long)(*UP) - B3)*(50000 >> oss);
	if (B7 < 0x80000000) p = (B7*2)/B4;
	else p = (B7/B4)*2;
	X1 = (p >> 8)*(p >> 8);
	X1 = (X1*3038) >> 16;
	X2 = (-7357*p) >> 16;
	*UP = p + ((X1+X2+3791) >> 4);
}
