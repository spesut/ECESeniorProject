#define F_CPU 8000000UL

#include <util/delay.h>
#include <avr/io.h>
#include "one_wire.h"

#define DECIMAL_TEMP 625	//Corresponds to 12-bit mode, or .0625 degrees C

int one_wire_reset(void)
{
	int check;
	
	//Pull the line low for 480 us
	OUTPUT_MODE();
	LOW();
	_delay_us(480);
	
	//Release line, and wait for 60 us
	INPUT_MODE();
	_delay_us(60);
	
	//Store the value on the line, and complete another 480 us period
	check = (DS18B20_PIN & (1<<DS18B20_DQ))>>3;
	_delay_us(420);
	
	//Return the value (0 presence received, 1 presence not received)
	return check;
	
}

void write_time_slot(char send)
{
	//Pull the line low for 1 us
	OUTPUT_MODE();
	LOW();
	_delay_us(1);
	
	//Sends a 1 if send=1
	if ((send & 0x01) == 1) {
		INPUT_MODE();
	}
	
	//Otherwise hold the line to send a zero
	_delay_us(60);
	INPUT_MODE();
}

char read_time_slot(void)
{
	char receive=0;
	
	//Pull the line low for 1 us
	OUTPUT_MODE();
	LOW();
	_delay_us(1);
	
	//Release the line and sample after 14 us
	INPUT_MODE();
	_delay_us(14);
	
	//Samples the line, and delays the remainder of the 60 us period
	if (DS18B20_PIN & (1<<DS18B20_DQ)) receive = 1;
	_delay_us(45);
	
	return receive;
}

void one_wire_write_byte(char byte)
{
	int i;
	
	//Writes one byte, LSB first
	for (i=0; i<8; i++) {
		write_time_slot(byte & 0x01);
		byte >>= 1;
		_delay_us(1);
	}
}

char one_wire_read_byte(void)
{
	int i;
	unsigned char data = 0;
	
	//Reads one byte, LSB first
	for (i=0; i<8; i++) {
		if (read_time_slot()) data |= (1<<i);
		_delay_us(1);
	}
	return data;
}

void read_temp_one_wire(temperature *temp)
{
	unsigned char temp_val[2];
	
	temp->degree = 0;
	temp->decimal = 0;
	
	//Only one device is used on the line, SKIPROM is issued
	//Issues a command to convert temp
	one_wire_reset();
	one_wire_write_byte(SKIPROM);
	one_wire_write_byte(CONVERTTEMP);
	
	//Conversion is complete when read_time_slot() returns 1 (< 750 ms)
	while(!read_time_slot());
	
	//Again, SKIPROM is used
	//Read the scratchpad
	one_wire_reset();
	one_wire_write_byte(SKIPROM);
	one_wire_write_byte(RSCRATCHPAD);
	
	//Reads the LSB and MSB of the temperature value
	temp_val[0] = one_wire_read_byte();
	temp_val[1] = one_wire_read_byte();
	
	//Only the first two scratchpad values are needed for temperature
	//The line is reset
	one_wire_reset();
	
	//Populates the tempearture structure
	temp->degree = (temp_val[1]<<8) | (temp_val[0]);
	temp->degree >>= 4;
	//Ensures no floating point math is needed
	temp->decimal = temp_val[0] & 0x0F;
	temp->decimal *= DECIMAL_TEMP;
}
