#ifndef __i2c_H__
#define __i2c_H__

#define GPS_AVR_ADDR 0x35

//Status register defines
#define STATUS_REG 	0xF8
#define START 		0x08
#define REP_START	0x10
#define DACK		0x50
#define DNACK		0x58




//Structure used to hold latitude/longitude coordinates
typedef struct {
	int degrees;
	long degrees_dec;
} coordinates;

//Issue start condition
void i2c_start(void);

//Issue stop condition
void i2c_stop(void);

//Issue a repeated start condition
void i2c_repstart(void);

//Writes one byte to the addressed slave
void i2c_write(unsigned char data);

//Reads one byte from the addressed slave
unsigned char i2c_read(unsigned char ack);

//Gets GPS coordinates from the GPS AVR
void get_GPS(coordinates *lat, coordinates *longit);

void error(void);

#endif
