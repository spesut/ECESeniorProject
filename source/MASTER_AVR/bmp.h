#ifndef __bmp_H__
#define __bmp_H__

//Defines for BMP085 addresses
#define BMP_READ_ADDR 0xEE
#define BMP_WRITE_ADDR 0xEF
#define BMP_CAL_DAT_ADDR 0xAA
#define BMP_CONROL_REG_ADDR 0xF4
#define BMP_DATA_REG_ADDR 0xF6
#define BMP_TEMP_CONTROL_REG 0x2E
#define BMP_PRESS_CONTROL_REG 0x34
#define EOC_PIN PC2
#define EOC_DDR DDRC
#define EOC_PIN_REG PINC


//Global variables for reading the calibration data from EEPROM
short AC1, AC2, AC3, B1, B2, MB, MC, MD;
unsigned short AC4, AC5, AC6;

//Reads in the calibration values from EEPROM
void bmp_init(void);

//Reads the uncompensated temperature value
long read_temp(void);

//Reads the uncompensated pressure value
long read_press(void);

//Compensates values of temperature and pressure
//utilizing the algorithm provided in the BMP085 Datasheet
void calc_data(long *UT, long *UP);

#endif
