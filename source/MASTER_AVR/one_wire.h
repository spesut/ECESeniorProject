// One-Wire Interface Routines

#ifndef __ONE_WIRE_H_
#define __ONE_WIRE_H_

//Pin Selection
#define DS18B20_PORT PORTC
#define DS18B20_DDR DDRC
#define DS18B20_PIN PINC
#define DS18B20_DQ PC3

//IO Defines
#define INPUT_MODE() DS18B20_DDR &= ~(1<<DS18B20_DQ)
#define OUTPUT_MODE() DS18B20_DDR |= (1<<DS18B20_DQ)
#define LOW() DS18B20_PORT &= ~(1<<DS18B20_DQ)
#define HIGH() DS18B20_PORT |= (1<<DS18B20_DQ)

//Available Commands
#define CONVERTTEMP 0x44
#define RSCRATCHPAD 0xBE
#define WSCRATCHPAD 0x4E
#define CPYSCRATCHPAD 0x48
#define RECEEPROM 0xB8
#define RPWRSUPPLY 0xB4
#define SEARCHROM 0xF0
#define READROM 0x33
#define MATCHROM 0x55
#define SKIPROM 0xCC
#define ALARMSEARCH 0xEC

#include <avr/io.h>

//Structure for holding temperature (used to avoid floating point operations)
typedef struct {
	int degree;
	int decimal;
} temperature;

//Resets the DS18B20
int one_wire_reset(void);

//Writes a 0 or 1 to the DS18B20
void write_time_slot(char send);

//Reads one bit from the DS18B20
char read_time_slot(void);

//Writes one byte to the DS18B20
void one_wire_write_byte(char byte);

//Reads one byte from the DS18B20
char one_wire_read_byte(void);

//Issues commands to convert temp and read the temp
void read_temp_one_wire(temperature *temp);

#endif // __ONE_WIRE_H_
