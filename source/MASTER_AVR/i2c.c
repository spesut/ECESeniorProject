#include <avr/io.h>

#include "i2c.h"

void i2c_start(void)
{
	//Send start condition
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)));	//Wait until bus becomes free
	if ((TWSR & STATUS_REG) != START) while(1);	//Checks for valid start condition.
}

void i2c_stop(void)
{
	//Sends a stop condition
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
}

void i2c_repstart(void)
{
	//Sends a repeated start
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	
	if ((TWSR & STATUS_REG) != REP_START) while(1);	//Checks for valid repeated start condition
}

void i2c_write(unsigned char data)
{
	//Writes one byte over the i2c line
	TWDR = data;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)));

}

unsigned char i2c_read(unsigned char ack)
{
	//Reads a byte over the i2c line
	//Either acknowledge or not (if end of transmission)
	unsigned char readdata;
	if (ack) {
		TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
		if ((TWSR & STATUS_REG) != DACK) while(1);	//ACK returned.
	}
	else {
		TWCR = (1<<TWINT)|(1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
		if ((TWSR & STATUS_REG) != DNACK) while(1);	//NACK returned.
	}
	readdata = TWDR;
	return readdata;
}

void error(void)
{
	while(1);
}

void get_GPS(coordinates *lat, coordinates *longit)
{
	//Initiates communication with the GPS AVR
	i2c_start();
	i2c_write(GPS_AVR_ADDR);

	//Values can be transmitted one following the other.  This bookkeeping is
	//taken care of in the SLAVE_TRANSMITTER code on the GPS AVR.	
	
	//Reads in latitude information
	lat->degrees = i2c_read(1);
	lat->degrees = ((lat->degrees << 8) | i2c_read(1));
	lat->degrees_dec = i2c_read(1);
	lat->degrees_dec = ((lat->degrees_dec << 8) | i2c_read(1));
	lat->degrees_dec = ((lat->degrees_dec << 8) | i2c_read(1));
	lat->degrees_dec = ((lat->degrees_dec << 8) | i2c_read(1));
	
	//Reads in longitude information
	longit->degrees = i2c_read(1);
	longit->degrees = ((longit->degrees << 8) | i2c_read(1));
	longit->degrees_dec = i2c_read(1);
	longit->degrees_dec = ((longit->degrees_dec << 8) | i2c_read(1));
	longit->degrees_dec = ((longit->degrees_dec << 8) | i2c_read(1));
	longit->degrees_dec = ((longit->degrees_dec << 8) | i2c_read(0));
	
	i2c_stop();
}
