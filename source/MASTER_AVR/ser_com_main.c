#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>

#include <util/delay.h>

#include "bmp.h"
#include "i2c.h"
#include "one_wire.h"

//Defines needed for data-logger switch
#define LOG_DATA_PIN PC1
#define LOG_DATA_DDR DDRC
#define LOG_DATA_PIN_REG PINC

//function declarations
void serial_init(void);
void write_serial (unsigned char key);
unsigned char read_serial (void);
void init_clk(void);

int main (void) {
	unsigned char key;		//temporary variable for transmitted key
	long temp = 0;
	long pressure = 0;
	coordinates latitude, longitude;
	temperature temp_val;
	char log_data;
	
	init_clk();				//Sets the clock to 8 MHz
	serial_init();			//initialize serial settings
	bmp_init();				//Reads all values necessary for calculating the compensated measurements
	
	LOG_DATA_DDR &= ~(1<<LOG_DATA_PIN);
	
	while (1) {
		key = read_serial();		//Gets a new key from the computer
		if (key == ',') {		//Starts the conversion if a comma is received
			temp = read_temp();	//Grabs uncompensated values of temperature and pressure
			pressure = read_press();
			
			get_GPS(&latitude, &longitude);	//Grabs GPS coords from other ATMega328p (via i2c)

			calc_data(&temp, &pressure);	//Compensates the raw values of temperature and pressure
			read_temp_one_wire(&temp_val);	//Gets temperature value from the DS18B20
			
			if (LOG_DATA_PIN_REG & (1<<LOG_DATA_PIN)) log_data = 1;	//Checks if the switch is closed
			else log_data = 0;

			/*write_serial(temperature);	//Sends temperature from BMP085
			write_serial(temperature>>8);	//NOT IN USE:  Measurement seems to 
			write_serial(temperature>>16);	//drift as time increases
			write_serial(temperature>>24);	*/
			
			write_serial(temp_val.degree);	//Sends temperature from DS18B20
			write_serial(temp_val.degree>>8);	
			write_serial(temp_val.decimal);	
			write_serial(temp_val.decimal>>8);
			
			write_serial(pressure);		//sends pressure value from BMP085
			write_serial(pressure>>8);
			write_serial(pressure>>16);
			write_serial(pressure>>24);
			
			write_serial(latitude.degrees);		//sends latitude coords to user
			write_serial(latitude.degrees>>8);
			write_serial(latitude.degrees_dec);		
			write_serial(latitude.degrees_dec>>8);
			write_serial(latitude.degrees_dec>>16);
			write_serial(latitude.degrees_dec>>24);
			
			write_serial(longitude.degrees);	//sends longitude coords to user
			write_serial(longitude.degrees>>8);			
			write_serial(longitude.degrees_dec);		
			write_serial(longitude.degrees_dec>>8);
			write_serial(longitude.degrees_dec>>16);
			write_serial(longitude.degrees_dec>>24);
			
			write_serial(log_data);			//This value is used to control when
											//a reading is recorded
		}
	}
	return 0;
}

//initialize serial settings
void serial_init(void)
{
	UBRR0H = 0;
	UBRR0L = 16;							//28800 baud for 8MHz system clock
	UCSR0B = (_BV(TXEN0) | _BV(RXEN0)); 	//enable tx and rx and bit
   	UCSR0C= (1<<USBS0)|(3<<UCSZ00);			// 8 bit no parity 2 stop
	UCSR0A |= (_BV(U2X0)); 					// double baud rate
}

//writes a character to the serial port
void write_serial (unsigned char key){
	while (!(UCSR0A & _BV(UDRE0))){}		//waits for a clear send buffer
	UDR0 = key;								//writes character
}

//reads a character from the serial port
unsigned char read_serial (void){	
	while (!(UCSR0A & _BV(RXC0))){}			//waits for an incoming character
	return UDR0;							//returns the incoming character
}

void init_clk(void)
{
	CLKPR = 0x80;				//Clears the CLKDIV bits, giving an 8 MHz clock
	CLKPR = 0x00;
}
