#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "lcd.h"
#include "printf.h"
#include "gps.h"
#include "i2c_ST.h"


//Function Declarations
void serial_init(void);
int write_serial (char key, FILE *unused);
int read_serial (FILE *unused);
void init_clk(void);

//Global variables to hold gps coordinates
//and a counter to determine what byte to send the other AVR
volatile uint8_t count = 0;
GPS_Data gps_data;

ISR(TWI_vect)
{
	cli();			// Disable Global Interrupt

	count = i2c_ST(&gps_data, count);
	if (count == 12) count = 0;

	sei();			// Reenable Global Interrupt 
}

void init(void)
{
	//Calls all initialization functions
	init_clk();
	lcd_init();
	serial_init();
	i2c_init();
	sei();
}

//Setup the serial port as an input file stream
static FILE uart_str = FDEV_SETUP_STREAM(NULL, read_serial, _FDEV_SETUP_READ);
//Setup the LCD as an output file stream
static FILE lcd_str = FDEV_SETUP_STREAM(lcd_putchar, NULL, _FDEV_SETUP_WRITE);

int main(void)
{
	init();

	//Buffer to hold the NMEA strings
	char key[200];
	int result = 0;
	
	//Assigns stdin to the serial connection with the GPS
	stdin = &uart_str;
	//Assigns stdout to the LCD
	stdout = &lcd_str;
	
	while(1) {
		//Grabs a line from the GPS
		fgets(key, 100, &uart_str);
		//Parses and checks the sentence is valid
		result = parseGPS(&gps_data, key);
		//If result was valid, print the data
		if (result == 0) print_coords(&gps_data);
	}
	return 0;
}


//initialize serial settings
void serial_init(void)
{
	UCSR0A = (1<<U2X0);			//Doubles the Baud rate
	UBRR0H = 0;
	UBRR0L = 16;				//28800 baud for 8MHz system clock
	UCSR0B = (_BV(TXEN0) | _BV(RXEN0)); 	//enable tx and rx and bit
   	UCSR0C = 3<<UCSZ00;			// 8 bit no parity 1 stop
   	
}

//writes a character to the serial port
int write_serial (char key, FILE *unused)
{
	while (!(UCSR0A & _BV(UDRE0))) ;	//waits for a clear send buffer
	UDR0 = key;				//writes character
	return 0;
}

//reads a character from the serial port
int read_serial (FILE *unused)
{	
	char c;
	while (!(UCSR0A & _BV(RXC0))) ;		//waits for an incoming character
	c = UDR0;				//returns the incoming character
	if (c == '\r') return '\n';
	else return c;
}

void init_clk(void)
{
	CLKPR = 0x80;				//Sets the clock prescalar to 1
	CLKPR = 0x00;				//This gives an 8MHz clock
}
