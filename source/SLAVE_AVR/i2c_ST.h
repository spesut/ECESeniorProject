#ifndef __i2c_ST_H__
#define __i2c_ST_H__


#include "gps.h"

//Initializes the AVR to be a slave transmitter
void i2c_init(void);

//Delivers the GPS information to the master receiver
int i2c_ST(GPS_Data *data, int count);

#endif	//__i2c_ST_H__
