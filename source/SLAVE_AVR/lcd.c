#include <avr/io.h>

#define F_CPU 8000000UL //8 MHz
#include <util/delay.h>

#include "lcd.h"

//16X2 LCD_MODE = 1, 16X1 LCD_MODE = 0
#define LCD_MODE 1

unsigned char dd_ram;
unsigned char addr;

void strobe(void) {
	//Toggles the enable bit on and off
	LCD_E = 1;
	LCD_E = 0;
}

unsigned char read_byte(void)
{
	unsigned char x;
	
	//Sets up to read from the LCD
	PORTB |= 0xF0;
	DDRB = 0x0F;
	LCD_RS = 0;
	LCD_RW = 1;

	//Enable read of upper 4 bits
	LCD_E = 1;
	
	asm volatile ("nop");
	asm volatile ("nop");
	
	//Reads upper 4 bits
	x = PINB & 0xF0;
	LCD_E = 0;
	
	//Enable read of lower 4 bits
	LCD_E = 1;
	
	asm volatile ("nop");
	asm volatile ("nop");
	
	//Reads lower 4 bits
	x |= (PINB & 0xF0) >> 4;

	LCD_E = 0;
	
	//Returns the pins to their original state
	DDRB = 0xFF;
	LCD_RW = 0;
	return x;
}

void send_nibble(unsigned char b) {
	//Sends the upper 4 bits of b

	PORTB = (PORTB & 0x0F) | (b & 0xF0);
	strobe();
}

void send_byte(unsigned char b) {
	send_nibble(b); 		//Send high nibble
	send_nibble(b << 4); 	//Send low nibble
}

void send_cmd(unsigned char b)
{
	//Sends a command to the LCD

	LCD_RS = 0; 	//Command mode
	send_byte(b);
	chk_bf();
}

void send_data(unsigned char b)
{
	//Sends a data byte to the LCD
	//Makes sure next write location is valid

	LCD_RS = 1; 	//Data mode
	send_byte(b);
	chk_bf();
	addr = read_byte();

	//Moves the next write location if the next write is off the LCD screen
#if (LCD_MODE)
		if (addr == 0x10) set_ddram(0x40);
		if (addr == 0x50) set_ddram(0x00);
#endif
	
#if (!LCD_MODE)
		if (addr == 0x08) set_ddram(0x40);
		if (addr == 0x48) set_ddram(0x47);
#endif
}

void lcd_init(void) {
	DDRB = 0xFF;
	_delay_ms(15);	//Delay for LCD to start itself up
	
	LCD_E = 0;
	LCD_RW = 0;
	LCD_RS = 0;
	
	//Start initializing the LCD
	send_nibble(FUNC_SET);	//Sends initial function setup (8 bit interface) 3 times
	_delay_us(4100);		//Delays called for by datasheet
	send_nibble(FUNC_SET);
	_delay_us(100);
	send_nibble(FUNC_SET);
	chk_bf();
	send_nibble(FBIT_FUNC); //4 bit interface
	chk_bf();
	
	send_cmd(FBIT_FUNC|NUM_LINES); //Function set: 4 bit interface, 2 line display
	send_cmd(DISP_OFF); 	//Turns display off
	send_cmd(LCD_CLEAR);	//Clears the display
	send_cmd(ENTRY_MODE); 	//Increments DDRAM on read/write, does not shift display
	send_cmd(DISP_ON); 		//Turns display on, cursor on, blink on
}

unsigned char chk_bf(void)
{	
	//This function returns the last address that was accessed
	//Reads ddram until the busy flag isn't set
	do {
		dd_ram = read_byte();
	} while (dd_ram & 0x80);
	
	return (dd_ram);
}

void set_ddram(unsigned char addr)
{
	//Writes a DDRAM address to the LCD
	LCD_RS = 0;
	LCD_RW = 0;

	send_byte(addr | 0x80);
	chk_bf();
}
