//Steven Pesut
//10/24/10
//Functions to parse GPS data and place it into a unique data structure


#include <stdio.h>
#include "gps.h"
#include "printf.h"
#include "lcd.h"

/**********************************************************************
$GPGGA,hhmmss.ss,ddmm.mmmm,a,dddmm.mmmm,b,q,xx,p.p,a.b,M,c.d,M,x.x,nnnn

hhmmss.ss   UTC of position
ddmm.mmmm   Latitude of position
a           N or S, latitutde hemisphere
dddmm.mmmm  Longitude of position
b           E or W, longitude hemisphere
q           GPS Quality indicator (0=No fix, 1=Non-differential GPS fix, 2=Differential GPS fix, 6=Estimated fix)
xx          Number of satellites in use
p.p         Horizontal dilution of precision
a.b         Antenna altitude above mean-sea-level
M           Units of antenna altitude, meters
c.d         Geoidal height
M           Units of geoidal height, meters
x.x         Age of Differential GPS data (seconds since last valid RTCM transmission)
nnnn        Differential reference station ID, 0000 to 1023

**********************************************************************/

int parseGPS(GPS_Data *data, char *NMEA_string)
{
	//Used to ensure the proper sentence is being decoded
	const char *code = "$GPGGA";
	int i;
	
	//Variables to hold number of commas and their indices
	int comma_pos[14] = {0};
	int comma_count = 0;
	
	//Initial values for parsing the sentence
	int NMEA_end = 0;
	int pos = 0;
	
	//Resets the checksum value
	int checksum_pos = 0;
	char checksum = 0;
	char checksum_flag = 0;
	
	//Temporary variables for calculating the decimal degrees
	int mins, mins_dec;
	
	//Used to get the quadrant (N/S, E/W)
	char quad;
	
	//Check for desired NMEA sentence
	for (i=0; i<6; i++) {
		if (NMEA_string[i] != code[i]) return 1;
	}
	
	//Grabs all relevant points in the GPS string (commas, newline, asterix)
	//These points are used to pull data from the string
	while (NMEA_string[pos] != '\n') {
		pos++;
		switch (NMEA_string[pos]) {
			case ',':
				comma_pos[comma_count] = pos;
				comma_count++;
				break;
			case '\n':
				NMEA_end = pos;
				break;
			case '*':
				checksum_flag = 1;
				checksum_pos = pos;
				break;
		}
		//Computes the checksum from the dollar sign to the asterix (exclusive)
		if (!checksum_flag) checksum ^= NMEA_string[pos];
	}
	
	//Checks for a valid NMEA sentence input
	if (comma_count != 14 || NMEA_end == 0 || checksum_pos == 0) return 2;
	
	//Makes sure the checksums match up
	if (str2int(NMEA_string, checksum_pos+1, 2) != checksum) return 3;
	
	//Get Latitude
	getnum(NMEA_string, comma_pos[1]+1, comma_pos[1]+2, &data->latitude.degrees);
	getnum(NMEA_string, comma_pos[1]+3, comma_pos[1]+4, &mins);
	getnum(NMEA_string, comma_pos[1]+6, comma_pos[2]-1, &mins_dec);
	quad = NMEA_string[comma_pos[2]+1];
	
	//Converts the minutes and decimal minutes to a decimal value.
	//Integer division is performed for more efficient calculation.
	data->latitude.degrees_dec = (long)(mins*1000000)/6 + (long)mins_dec*100/6;
	if (quad == 'S' || quad == 's') data->latitude.degrees *= -1;

	//Get Longitude
	getnum(NMEA_string, comma_pos[3]+1, comma_pos[3]+3, &data->longitude.degrees);
	getnum(NMEA_string, comma_pos[3]+4, comma_pos[3]+5, &mins);
	getnum(NMEA_string, comma_pos[3]+7, comma_pos[4]-1, &mins_dec);
	quad = NMEA_string[comma_pos[4]+1];
	
	//Converts the minutes and decimal minutes to a decimal value.
	//Integer division is performed for more efficient calculation.
	data->longitude.degrees_dec = (long)(mins*1000000)/6 + (long)mins_dec*100/6;
	if (quad == 'W' || quad == 'w') data->longitude.degrees *= -1;
	
	//Get Number of Satellites
	getnum(NMEA_string, comma_pos[6]+1, comma_pos[7]-1, &data->num_sats);
	
	//Get Fix Quality
	getnum(NMEA_string, comma_pos[5]+1, comma_pos[6]-1, &data->quality);
	
	return 0;
}

int str2int(char *NMEA_string, int index, int strlength)
{
	//Function converts a hexadecimal ascii string into a decimal value
	//Returns the decimal value
	int val = 0;
	while (strlength > 0) {
		//Checks the character to see what value normalizes it
		if (NMEA_string[index] > '9') val = (val<<4) + NMEA_string[index] - 0x37;
		else val = (val<<4) + NMEA_string[index] - 0x30;
		
		index++;
		strlength--;
	}
	return val;
}

int getnum(char *NMEA_string, int start_index, int end_index, int *var)
{
	//Function converts an ascii string into a decimal value
	//Zero is returned on success
	int val = 0;
	while (start_index <= end_index) {
		//Check for valid input between 0 and 9
		if (NMEA_string[start_index] < '0' || NMEA_string[start_index] > '9') return 4;
		
		//Adjust the output value
		val = 10*val + NMEA_string[start_index] - 0x30;
		start_index++;
	}
	*var = val;
	return 0;
}

void print_coords(GPS_Data *data)
{
	//Homes the display
	send_cmd(0x01);
	
	//Only prints the values if there is a valid fix
	if (data->quality == 0) printf("No fix\nSats: %d", data->num_sats);
	else {
		printf("Lat:%d.%ld\n", data->latitude.degrees, data->latitude.degrees_dec);
		printf("Lng:%d.%ld", data->longitude.degrees, data->longitude.degrees_dec);
	}
}
