#include <avr/io.h>

#include "i2c_ST.h"
#include "gps.h"

//Address the AVR responds to.
#define addr 0x34

void i2c_init(void)
{
	//Initializes the ST with an address
	TWAR = addr;
	TWCR = (1<<TWEA)|(1<<TWEN)|(1<<TWIE);
}

int i2c_ST(GPS_Data *data, int count)
{
	unsigned char twi_status; 	
	twi_status= TWSR & 0xF8; //Get TWI StatusReg 

	switch(twi_status) {		
		case 0xA8:  //SLA+R received 
			TWDR = (data->latitude.degrees)>>8;
			
			TWCR |= (1<<TWINT)|(1<<TWEA);// Clear TWINT 
			count++;
			break;      

		case 0xB8: // one byte sent, ACK received
			switch(count) {		//Switch/case allows the AVR to send the correct
								//data to the other AVR
				case 1:		
					TWDR = data->latitude.degrees;
					break;
				case 2:
					TWDR = (data->latitude.degrees_dec)>>24;
					break;
				case 3:
					TWDR = (data->latitude.degrees_dec)>>16;
					break;
				case 4:
					TWDR = (data->latitude.degrees_dec)>>8;
					break;
				case 5:
					TWDR = data->latitude.degrees_dec;
					break;
				case 6:
					TWDR = (data->longitude.degrees)>>8;
					break;
				case 7:
					TWDR = data->longitude.degrees;	
					break;
				case 8:
					TWDR = (data->longitude.degrees_dec)>>24;
					break;
				case 9:
					TWDR = (data->longitude.degrees_dec)>>16;
					break;
				case 10:
					TWDR = (data->longitude.degrees_dec)>>8;
					break;
				case 11:
					TWDR = data->longitude.degrees_dec;
					break;
			}
			TWCR |= (1<<TWINT)|(1<<TWEA); //Clear TWINT 
			count++;
			break; 

		case 0xC0: // one byte sent NACK received
			TWCR |= (1<<TWINT); //Clear TWINT 
			break; 

        default: 
    		TWCR |= (1<<TWINT);   // Clear TWINT 
	}
	return count;	//Returns what byte should be sent next
}
