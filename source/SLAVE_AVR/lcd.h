#ifndef __LCD_H__
#define __LCD_H__

//Sets up a macro to define a pin
//&rg is cast to a pointer to an _io_reg
//This pointer is then dereferenced to a bit value
//The ## operator basically pastes whatever value bt
//gets onto bit.  Therefore, it dereferences a pointer to
// a register, rg, and finds the bit given at bt
#define REGISTER_BIT(rg,bt) ((volatile _io_reg*)&rg)->bit##bt 

//Defines pin location for Register select, Read/Write, Clock
#define LCD_RS  REGISTER_BIT(PORTB,0)
#define LCD_RW  REGISTER_BIT(PORTB,1)
#define LCD_E  	REGISTER_BIT(PORTB,2)

// LCD DATA PORT B Pins 4-7

// LCD commands
#define FUNC_SET	0x30	// Intialization Function Set
#define FBIT_FUNC	0x20	// 4 Bit Function set
#define LCD_CLEAR	0x01	// LCD Clear
#define LCD_HOME	0x02	// LCD Home
#define DISP_OFF	0x08	// Turns the display off
#define DISP_ON		0x0F	// Turns the display on, cursor on, blink on
#define NUM_LINES	0x08	// 0x08 for num lines = 2
#define ENTRY_MODE	0x06	// Increments DDRAM on read/write, does not shift display

//Struct used to define pins as LCD_RS, LCD_RW and LCD_E
typedef struct 
{ 
  unsigned int bit0:1; 
  unsigned int bit1:1; 
  unsigned int bit2:1; 
  unsigned int bit3:1; 
  unsigned int bit4:1; 
  unsigned int bit5:1; 
  unsigned int bit6:1; 
  unsigned int bit7:1; 
} _io_reg; 



// LCD initialization
void lcd_init(void);

//Toggles Enable bit on/off
void strobe(void);

//Reads a byte from the LCD
unsigned char read_byte(void);

//Sends 4 bits to the LCD
void send_nibble(unsigned char b);

//Sends a byte (upper nibble first) to the LCD
void send_byte(unsigned char b);

//Writes a function command to the LCD
void send_cmd(unsigned char b) ;

//Writes character data to the LCD
void send_data(unsigned char b);

//Checks if the LCDs busy flag is set
unsigned char chk_bf(void);

//Sets the address that the next LCD write will occurs
void set_ddram(unsigned char addr);

#endif // __LCD_H__
