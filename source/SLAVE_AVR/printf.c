#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <util/delay.h>
#include <stdio.h>
#include "lcd.h"

//For a 16X2 LCD_MODE = 1, for 16x1, LCD_MODE = 0
#define LCD_MODE 1

//Function format needed to create the file stream
int lcd_putchar(char c, FILE *unused)
{
	//This function checks for a newline and handle how to respond
	//If a newline is not passed, it passes the character to the LCD

	unsigned char addr;
	if (c == '\n') {
#if (LCD_MODE)
		addr = chk_bf();
		if (addr & 0x40) send_cmd(LCD_CLEAR);
		else set_ddram(0x40);
#endif

#if (!LCD_MODE)
		send_cmd(LCD_CLEAR);
#endif		
	}
	else {
		send_data(c);
	}
	return 0;
}
