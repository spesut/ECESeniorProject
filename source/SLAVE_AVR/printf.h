// Printf routine

#ifndef __PRINTF_H
#define __PRINTF_H

//Low level function that will be used by printf in stdio.h
//Prints characters to the LCD display
int lcd_putchar(char c, FILE *unused);

#endif // __PRINTF_H
