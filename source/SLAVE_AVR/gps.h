#ifndef __GPS_H__
#define __GPS_H__

//Struct for the coordinate data
typedef struct {
	int degrees;
	long degrees_dec;
} coordinates;

//Structure to hold all relevant GPS data
typedef struct {
	coordinates latitude;
	coordinates longitude;
	int quality;
	int num_sats;
	int checksum;
} GPS_Data;

//Converts a hex ascii string to a decimal and returns it
int str2int(char *NMEA_string, int index, int strlength);

//Converts an ascii string to a decimal
int getnum(char *NMEA_string, int start_index, int end_index, int *var);

//Parses a NMEA sentence and gathers relevant data from the string
int parseGPS(GPS_Data *data, char *NMEA_string);

//Prints the GPS coordinates to an LCD
void print_coords(GPS_Data *data);

#endif
