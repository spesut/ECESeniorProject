ECESeniorProject (May 2011)
================

Source code for a data-logging weather station.  The circuit consists of two Atmel microprocessors communicating with a number of different sensors.  The sensors interfaced are a GPS receiver, a temperature sensor and a pressure sensor.  All measurements are collected and logged to a computer using the RS232 protocol.  The device is able to read and log pressure values within +/- 2% (between 960 mBars and 1030 mBars) and temperature values within +/- 5% degF +/- 1 degF (between 10 degF and 90 degF).  Data is written to the computer in a Google-Earth compatible format, which enables the device to easily illustrate the collected readings.

The source code is separated into three directories (PC, MASTER_AVR and SLAVE_AVR).  Please see the block diagram and schematic for details.

AVR Build Dependencies:
	avr-gcc
	avr-binutils
	avr-libc
	avrdude
